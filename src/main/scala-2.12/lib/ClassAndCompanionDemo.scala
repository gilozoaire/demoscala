package lib

/**
  * Created by majorgilles on 12/22/16.
  */
// classes and companion objects
class ClassAndCompanionDemo(someProp: String /*private, immutable*/, var someMutableProp: String /*getter and setters */, val someImmutableProp: Int = 5 /*val = getter only , default value = 5*/) {

  // where is my constructor??? see above, you actually have two!
  // where are my accessors??? see above, depends on var/val/nothing

  // Lazy and strict fields
  var someField: String = "hello"
  lazy val someLazyField: Int = "some long string".length // lazy val/vars are computed only when necessary (e.g. printing)

  // instance methods (static methods and properties go in companion object)
  def someLazyMethod: () => Int = () => 5
  def someStrictMethod: Int = 5
}

// Companion object singleton for ClassAndCompanionDemo class
object ClassAndCompanionDemo {
  // Apply is idiomatic scala: way to generate a new object without new by using the companion object apply to generate instances
  def apply(someProp: String, someMut: String, someImmut: Int): ClassAndCompanionDemo = new ClassAndCompanionDemo(someProp, someMut, someImmut)

  def someOtherStaticMethod : String = "HELLO"
}



