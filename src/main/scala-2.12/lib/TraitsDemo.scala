package lib

/**
  * Created by majorgilles on 12/22/16.
  */

trait JsonConvertible {

  def someAbstractMethod: String // Empty methods, similar to java interfaces

  // method with an implementation
  def toJson: String = {
    val fieldPairs = for (field <- getClass.getDeclaredFields) yield {
      field.setAccessible(true)
      (field.getName, field.get(this).toString)
    }

    var jsonString = fieldPairs.foldLeft("{ ")((acc, elem) => acc + s"${elem._1} : ${elem._2}, ")
    jsonString = jsonString.patch(jsonString.lastIndexOf(","), "", 1)
    jsonString + "}"
  }

}

class TestPerson(val fName: String, val lName: String, val age: Float) extends JsonConvertible {
  override def someAbstractMethod: String = "I'm a person, not a number"
}
class TestBook(val author: String, val title: String, val numPages: Int) extends JsonConvertible {
  override def someAbstractMethod: String = "I'm a book"
}


// Algebraic data types: sealed traits and case classes / objects
// Think of it as an enum of classes: Several classes are grouped under the enum. Very useful for type safety, generic code...

// Case classes have autogeneratad Apply methods. Thus, they commonly used for struct-like data types.
// http://docs.scala-lang.org/tutorials/tour/case-classes.html
case class Point(x: Float, y: Float)

sealed trait Shape  {
  // some methods useful to all shapes, e.g. compute the area
  def computeArea : Double
}
final case class Circle(radius: Float, var origin: Point) extends Shape {
  def computeArea: Double = math.Pi * math.pow(radius, 2f)
}
final case class Rectangle(h: Float, w: Float, var origin: Point) extends Shape {
  def computeArea: Double = h * w
}