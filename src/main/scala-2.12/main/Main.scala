package main

import lib._

/**
  * Created by majorgilles on 12/22/16.
  */
object Main extends App {

  // Note that companionExample doesn't need an explicit type, it's inferred by the scala compiler
  val companionExample = ClassAndCompanionDemo("tEST TEST", "test mut", 23)

  //companionExample.someProp // doesnt compile: no val / var == private
  println(companionExample.someMutableProp)
  companionExample.someMutableProp = "hello" // ok, generated getter and setters with var
  println(companionExample.someMutableProp)
  //companionExample.someImmutableProp = 25 // doesn't compile -> val == immutable with generated getter

  // Trait examples
  val person = new TestPerson("Gilles", "Major", 32f)
  val book = new TestBook("Tolkien", "LOTR", 322)

  /*println(person.toJson)
  println(book.toJson)*/

  // Pattern matching : "switch on steroids"
  def shapeFunc(shape: Shape): String = shape match {
    case Rectangle(h, w, o) => s"Got rectangle with height: $h, width: $w, origin: $o"
    case Circle(r, o) => s"Got circle with radius: $r, origin: $o"
  }
  /*println (shapeFunc(Rectangle(25, 23, Point(2, 3))))
  println (shapeFunc(Circle(25, Point(2, 3))))*/

  // You can similarly pattern match and deconstruct all types in scala (lists, options, arrays...)
  val optionNumber = Some(5)
  val optionNumberNone: Option[Int] = None
  def someFuncOnOptionals(o: Option[Int]) = o match {
    case Some(x) => println(x)
    case None => println("Pattern matched against empty option!")
  }
  //someFuncOnOptionals(optionNumber)
  //someFuncOnOptionals(optionNumberNone)

  // Beware of partial functions when matching, or you will get an exception! => you need to match every possible configuration of the input
  def dangerousMatch(str: String): Unit = str match {
    case "HELLO" => println("hello")
  }
  //dangerousMatch("IM DANGEROUS AND WILL CAUSE A CRASH")
  def safeVersionOfDangerousMatch(str: String): Unit = str match {
    case "HELLO" => println("hello")
    case _ => println("Hey I'm not matched against!")
  }
  //safeVersionOfDangerousMatch("I'm not dangerous anymore and will not cause a crash :(")

  // Functional programming: absolute basics
  val c1 = Circle(2, Point(2,2))
  val c2 = Circle(2, Point(5, 3))
  val rect = Rectangle(40, 30, Point(1, 2))
  val listOfShapes: List[Shape] = List[Shape] (c1, rect, c2)

  // map
  val listofStrings = listOfShapes.map(shapeFunc) // we pass the shapeFunc function to map each shape to a string
  /*
  Can also be written: val listofStrings = listOfShapes.map((s: Shape) => shapeFunc(s))
  but above syntax is better since we have a named function
  */
  //println(listofStrings)

  // filter
  var shapesWithBigArea = listOfShapes.filter(s => s.computeArea > 35)
  //println(shapesWithBigArea)

  // reduce
  val scores = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
  val productOfScores = scores.reduce(_ * _)
  //print(productOfScores)
}
